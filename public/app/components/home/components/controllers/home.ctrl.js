(function () {
	angular
		.module('app')
		.controller('HomeCtrl', ['$http', '$window', HomeCtrl]);

		function HomeCtrl ($http, $window) {
			var self = this;

			self.sendUrl = function sendUrl () {
				if (self.makeItShort === "" || self.makeItShort === undefined) {
					self.link = "Please specify the link.";
				}

				if (self.makeItShort) {
					$http.post('/data', {mainURL: self.makeItShort})
					.then(function success (res) {
						self.link = 'http://localhost:3010/' + res.data;
						self.result = res.data;
						console.log(res.data);
					}, function error (err) {
						console.log(err);
					});
				}
			};

			self.goToLink = function goToLink () {
				$http.get('/' + self.result)
					.then(function success (res) {
						$window.open(res.data);
					}, function error (err) {
						console.log(err);
					});
			};
		}
}());