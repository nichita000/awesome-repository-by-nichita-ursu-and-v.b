(function () {
	angular
		.module('app')
		.controller('AllGeneratedLinks', ['$http', AllGeneratedLinks]);

		function AllGeneratedLinks ($http) {
			var self = this;

			self.allLinks = [];

			self.getAllLinks = function getAllLinks () {
				$http.get('/data')
					.then(function success (res) {
						self.allLinks = res.data;
						console.log(res.data);
					}, function error (err) {
						console.log(err);
				});
			};

			self.getAllLinks();

			self.deleteLinkById = function deleteLinkById (hash, index) {
				$http.delete('/data/' + hash)
					.then(function success (res) {
						self.allLinks.splice(index, 1);
					}, function error (err) {
						console.log(err);
					});
			};

		}
}());