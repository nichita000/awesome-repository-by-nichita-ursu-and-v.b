(function () {
		angular
			.module('app')
			.controller('MainCtrl', ['$state', MainCtrl]);

			function MainCtrl ($state) {
				var self = this;

				$state.go('main.home');
			}
}());