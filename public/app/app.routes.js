(function () {
	angular
		.module('app')
		.config(routes);

		function routes ($stateProvider, $urlRouterProvider) {

			$stateProvider
				.state('main', {
					url: '/main',
					controller: 'MainCtrl',
					controllerAs: 'main',
					templateUrl: 'app/components/main/components/templates/main.tpl.html'
				})
				.state('main.home', {
					url: '/home',
					controller: 'HomeCtrl',
					controllerAs: 'home',
					templateUrl: 'app/components/home/components/templates/home.tpl.html'
				})
				.state('main.allLinks', {
					url: '/allLinks',
					controller: 'AllGeneratedLinks',
					controllerAs: 'allGenerated',
					templateUrl: 'app/components/allGenerated/components/templates/allGenerated.tpl.html'
				});

			$urlRouterProvider.otherwise('main');
		}
}());