var database = [];
var uuid = require('uuid');

module.exports.getDataApi = function getDataApi (req, res) {
    res.send(database);
};

module.exports.addDataApi = function addDataApi (req, res) {
    var mainURL = req.body.mainURL;
    var shortURL = uuid.v4();

    if (mainURL) {
        database.push({
            mainURL: mainURL,
            shortURL: shortURL
        });
    }

    res.send(shortURL);
};

module.exports.getDataByIdApi = function getDataByIdApi (req, res) {
    var shortURL = req.params.id;
    var flag = false;
    var order = i;

    for (var i = 0; i < database.length; i++) {
        if (database[i].shortURL === shortURL) {
            flag = true;
            order = i;
        }
    }

    if (flag) {
        res.send(database[order].mainURL);
    } else {
        res.status(404).send('wrong');
    }
};

module.exports.deleteDataByIdApi = function deleteDataByIdApi (req, res) {
    var shortURL = req.params.id;

    for (var i = 0; i < database.length; i++) {
        if(database[i].shortURL === shortURL) {
            database.splice(i , 1);
        }
    }

    res.send(database);
}
