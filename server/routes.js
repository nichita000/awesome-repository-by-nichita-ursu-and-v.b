module.exports = function (app) {
    var main = require('./app/controllers/main.ctrl');

    app.get('/data', main.getDataApi);
    app.get('/:id', main.getDataByIdApi);
    app.post('/data', main.addDataApi);
    app.delete('/data/:id', main.deleteDataByIdApi)
};
